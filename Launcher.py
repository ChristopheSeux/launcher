import sys, yaml
from os.path import join,dirname, abspath
import os
import subprocess
from functools import partial
from utils import *
import json

sys.path.append('/s/python/virtualenv36/lib/python3.6/site-packages')

from PySide2 import QtWidgets
from PySide2 import QtGui
from PySide2.QtWidgets import (QLineEdit, QPushButton, QApplication,
    QVBoxLayout, QHBoxLayout, QWidget, QSpacerItem, QComboBox, QFrame)
from PySide2.QtGui import QPixmap, QPalette
from PySide2.QtCore import QSize, Qt

import yamlordereddictloader

os.chdir(dirname(__file__))

icons_dir = abspath('icons')


softs_info = read_yml(abspath('softs.yml'))

## Get blender version
softs_info['blender']['versions'] = [*get_blender_release(), 'Last Build']

style = read_css(abspath('style.css'))


envs =[]
for env in [f for f in os.scandir(abspath('env')) if f.name.endswith('.yml') and not f.name.startswith('_')] :
    with open(env.path, 'r') as f:
        envs.append(yaml.load(f, Loader = yamlordereddictloader.Loader))

#print(envs)
class TopBar(QWidget) :
    def __init__(self):
        super().__init__()

        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)


        self.frame = QFrame()
        self.main_layout = QHBoxLayout()
        self.main_layout.setContentsMargins(10,10,10,10)

        self.frame.setLayout(self.main_layout)

        self.cbb_env = QComboBox()

        style = """QWidget {
            color:rgb(230, 230, 230);
            background-color: rgb(54, 57, 63);}
            QComboBox{
            font-size: 14px;
            font-weight: 520;
            background-color: rgb(54, 57, 63);}"""

        self.cbb_env.setStyleSheet(style)

        for env in envs :
            self.cbb_env.addItem(env['PROJECT_NAME'])

        self.main_layout.addWidget(self.cbb_env)
        self.main_layout.addStretch()


        palette = QPalette()
        palette.setColor(QPalette.Background, Qt.black)
        self.frame.setAutoFillBackground(True)
        self.frame.setPalette(palette)

        self.layout.addWidget(self.frame)

        self.setLayout(self.layout)

        self.frame.setStyleSheet('QFrame{background-color : rgb(34,36,39);}')

        #self.search = QLineEdit()
        #self.layout.addWidget(self.search)

class BtnSoft(QWidget) :
    def __init__(self, soft, infos):
        super().__init__()

        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.setLayout(self.layout)

        self.btn = QPushButton(' '+soft.title(),self)
        self.btn.setFixedHeight(30)
        self.btn.setIconSize(QSize(24, 24))
        self.cmd = abspath(infos['cmd'])

        self.layout.addWidget(self.btn)

        if 'versions' in infos and len(infos['versions']) > 1 :
            self.cbb_version = QComboBox()
            for v in infos['versions'] :
                self.cbb_version.addItem(v)

            self.cbb_version.setFixedSize(90, 30)
            self.layout.addWidget(self.cbb_version)

            if 'Last Build' in infos['versions'] :
                i = self.cbb_version.findText('Last Build')
                self.cbb_version.setCurrentIndex(i)


        else :
            frame = QFrame()
            frame.setFixedSize(90, 30)
            self.layout.addWidget(frame)

        icon = QPixmap(abspath(infos['icon']))
        self.btn.setIcon(icon)








class Window(QWidget):
    def __init__(self):
        super().__init__()

        self.setStyleSheet(style)

        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)

        # TOP BAR
        self.topbar = TopBar()
        #self.topbar.setFixedHeight(50)



        #self.search = QLineEdit()

        #topbar_layout.addWidget(self.cbb_env)
        #topbar_layout.addWidget(self.search)

        #topbar_layout.setAutoFillBackground(True)

        self.softs_btn_layout = QVBoxLayout()
        self.softs_btn_layout.setContentsMargins(10,10,10,20)

        self.softs_btn = []
        for soft,soft_info in softs_info.items() :
            btn_soft = BtnSoft(soft = soft, infos = soft_info)
            btn_soft.btn.clicked.connect(partial(self.launch, btn_soft))

            self.softs_btn.append(btn_soft)
            self.softs_btn_layout.addWidget(btn_soft)



        self.layout.addWidget(self.topbar)
        self.layout.addStretch()
        self.layout.addLayout(self.softs_btn_layout)
        self.layout.addStretch()
        # Set dialog layout
        self.setLayout(self.layout)
        # Add button signal to greetings slot
        #self.button.clicked.connect(self.greetings)

        #self.topbar.cbb_env.currentIndexChanged.connect(self.filter_softs)

    def filter_softs(self) :
        #self.softs_btn_layout.clear()
        for btn_soft in self.softs_btn :
            if self.cbb_env.currentText() in btn_soft.envs or 'global' in btn_soft.envs:
                btn_soft.show()
            else :
                btn_soft.hide()
    # Greets the user
    def greetings(self):
        print ("Hello %s" % self.edit.text())

    def launch(self,btn) :
        print(btn)
        args = json.dumps({'env' : self.topbar.cbb_env.currentText(), 'version' : btn.cbb_version.currentText()})
        subprocess.call(['python3',btn.cmd, args])


if __name__ == '__main__':
    # Create the Qt Application
    app = QApplication(sys.argv)
    # Create and show the form
    window = Window()
    window.show()
    # Run the main Qt loop
    sys.exit(app.exec_())
