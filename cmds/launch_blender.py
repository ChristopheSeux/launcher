import os,shutil
from os.path import join, basename, exists, dirname
import subprocess
import urllib.request
import ssl
import re
import zipfile
import bz2
import tempfile
import tarfile
import json
import sys

args = json.loads(sys.argv[-1])
version = args['version']

if version== '2.79' :
    os.environ['BLENDER_USER_SCRIPTS'] = '/s/blender/scripts'
    os.environ['PYTHONPATH'] = '/s/python:/s/python/virtualenv/lib/python3.5/site-packages'

    os.environ['STORE'] = '/z/___SERIE/NON-NON'
    os.environ['TEMPLATES'] = 'global:tvshow:nn'

else :
    os.environ['BLENDER_USER_SCRIPTS'] = '/s/blender/2.8_scripts'
    os.environ['PYTHONPATH'] = '/s/python:/s/python/virtualenv37/lib/python3.7/site-packages'
#os.environ['PYTHONHOME']= '/s/python/virtualenv37/lib/python3.7/site-packages'



ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
softs_dir = '/opt'
#if not exists(softs_dir) :  os.mkdir(softs_dir)


# Check for existing release

if version != 'Last Build' :
    url = 'https://download.blender.org/release/Blender%s'%version

    print(url)
    response = urllib.request.urlopen(url, context = ssl_context)
    data = response.read()
    body = data.decode("utf-8")

    match = re.findall(r'<a href=\"(.*linux.*)">', body)
    bname = [m for m in match if 'x86_64' in m and not re.search(r'rc\d',m)][-1]

    real_version = re.findall(r'\d\.\d{2}[a-z]?', bname)[0]

    blender_dir_name = 'blender-%s'%real_version
    blender_path = join(softs_dir,blender_dir_name,'blender')
    blender_dir = join(softs_dir,blender_dir_name)
    if not exists(blender_path) :
        download_url = url+'/' + bname

        print('download_url',download_url)
        download_path = join(tempfile.gettempdir(),basename(download_url))
        with urllib.request.urlopen(download_url) as response, open(download_path, 'wb') as out_file:
            shutil.copyfileobj(response, out_file)

        with tarfile.open(download_path, "r:bz2") as tar:
            tar.extractall(softs_dir)
            shutil.move(join(softs_dir,basename(download_path).replace('.tar.bz2','')),blender_dir)

# Get last build
elif version == 'Last Build' :
    burl = "https://builder.blender.org/"
    response = urllib.request.urlopen(burl,context= ssl_context)
    data = response.read()
    body = data.decode("utf-8")
    match = re.search(r'/download/blender-\d\.\d{2}-(.{4,20})-linux-.{2,20}-x86_64.tar.bz2',body)

    hash = match.group(1)
    download_url = burl + match.group(0)
    print('hash',hash)


    real_version = re.findall(r'\d\.\d{2}[a-z]?', match.group(0))[0]

    blender_dir_name = 'blender-%s'%real_version
    blender_path = join(softs_dir,blender_dir_name,'blender')
    blender_dir = join(softs_dir,blender_dir_name)


    current_hash = ''
    if exists(blender_path) :
        process = subprocess.Popen([blender_path,'--version'],stdout = subprocess.PIPE)
        stdout = process.communicate()
        stdout = stdout[0].decode('utf-8').strip('\n').split('\n')

        #version = stdout[0]
        bl_info = {k.strip('\t').replace(' ','_'):v.strip() for k,v in [i.split(':',1) for i in stdout[1:]]}

        current_hash = bl_info['build_hash']
        print('current_hash',current_hash)


    if current_hash != hash :
        print('Check for the new buildbot version')

        blender_dir = join(softs_dir,blender_dir_name)
        if os.path.exists(blender_dir) :
            print('Remove blender dir')
            shutil.rmtree(blender_dir)

        download_path = join(tempfile.gettempdir(),basename(download_url))
        if not os.path.exists(download_path) :
            print('download_url',download_url)
            with urllib.request.urlopen(download_url) as response, open(download_path, 'wb') as out_file:
                shutil.copyfileobj(response, out_file)

        ## UNZIP
        try :
            with tarfile.open(download_path, "r:bz2") as tar:
                tar.extractall(softs_dir)
                shutil.move(join(softs_dir,basename(download_path).replace('.tar.bz2','')),blender_dir)
        except Exception as e :
            print('The download file is corrupted\n',e)
            os.remove(download_path)

            print('download_url',download_url)
            with urllib.request.urlopen(download_url) as response, open(download_path, 'wb') as out_file:
                shutil.copyfileobj(response, out_file)







# Launch
subprocess.Popen(blender_path, shell=True)
