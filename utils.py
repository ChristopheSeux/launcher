import sys
import urllib.request
import ssl
import re

sys.path.append('/s/python/virtualenv36/lib/python3.6/site-packages')
import yaml
import yamlordereddictloader

def read_yml(path) :
    with open(path, 'r') as f:
        softs_info = yaml.load(f, Loader = yamlordereddictloader.Loader)

    return softs_info

def read_css(path) :
    with open(path, 'r') as f:
        style = f.read()

    return style


## Check blender version
def get_blender_release(version= '2.79') :
    url = 'https://download.blender.org/release/'

    ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    response = urllib.request.urlopen(url, context = ssl_context)
    data = response.read()
    body = data.decode("utf-8")

    match = re.findall(r'Blender\d\.\d{2}', body)
    match = [re.sub(r'^Blender','',m) for m in match]
    match= sorted(list(set(match)))

    index = match.index(version)

    return match[index:]
